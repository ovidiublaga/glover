<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);
Route::get('/', 'PagesController@home');
Route::get('/devino-glover', 'PagesController@devinoGlover');
Route::get('/despre-noi', 'PagesController@despreNoi');
Route::get('/despre-glovo', 'PagesController@about');
Route::get('/services', 'PagesController@services');
Route::get('/contact', 'PagesController@contact');

Route::resource('posts', 'PostsController');
Route::resource('documente', 'DocumentsController');
Route::resource('documentemasina', 'DocumentemasinaController');
Auth::routes();

Route::get('/home', 'DashboardController@administrare')->name('home');



Route::get('/protectiamuncii', 'DashboardController@protectiamuncii');
Route::get('/moddelivrare', 'DashboardController@moddelivrare');
Route::get('/contractdemunca', 'DashboardController@contractdemunca');
Route::get('/editeazainformatiile', 'DashboardController@editeazainformatiile');
Route::get('/solicitaadeverintasalariat', 'DashboardController@solicitaadeverintasalariat');
Route::get('/actiunicartedemunca', 'DashboardController@actiunicartedemunca');
Route::get('/solicitadeciziadeincetare', 'DashboardController@solicitadeciziadeincetare');
Route::get('/administrare', 'DashboardController@administrare');
Route::get('/administrare/documentele-mele', 'DashboardController@documentelemele');
Route::post('/administrare/documentele-mele/incarcaDocument', 'DashboardController@incarcaDocument');
Route::get('/administrare/iban', 'DashboardController@iban');
Route::post('/administrare/iban/schimba', 'DashboardController@schimbaIban');
Route::get('/test', 'DashboardController@test');


Route::get('/documente/utilizatori/{user_id}/{file}', 'DashboardController@secureFile');