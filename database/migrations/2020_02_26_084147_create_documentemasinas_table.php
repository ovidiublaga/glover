<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentemasinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentemasina', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('buletin_proprietar_masina');
            $table->string('asigurare_masina');
            $table->string('certificat_inmatriculare');
            $table->string('contract_de_comodat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentemasina');
    }
}
