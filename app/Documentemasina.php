<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentemasina extends Model
{
    protected $table = 'documentemasina';
    public function user() {
        return $this->belongsTo('App\User');
    }
}
