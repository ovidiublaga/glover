<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Documentemasina;

class DocumentemasinaController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentemasina = Documentemasina::orderBy('created_at', 'desc')->get();
        return view('documentemasina.index')->with('documentemasina', $documentemasina);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documentemasina.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'buletin_proprietar_masina' => 'image|nullable|max:1999',
            'asigurare_masina'          => 'image|nullable|max:1999',
            'certificat_inmatriculare'  => 'image|nullable|max:1999',
            'contract_de_comodat'       => 'image|nullable|max:1999'
        ]);

               // Handle file upload
        if ($request->hasFile('buletin_proprietar_masina')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('buletin_proprietar_masina')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('buletin_proprietar_masina')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreBuletinProprietar = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('buletin_proprietar_masina')->storeAs('public/dcsmm', $fileNameToStoreBuletinProprietar);
        } else {
            $fileNameToStoreComodat = 'noimage.jpg';
        }
        
        if ($request->hasFile('asigurare_masina')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('asigurare_masina')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('asigurare_masina')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreAsigurareMasina = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('asigurare_masina')->storeAs('public/dcsmm', $fileNameToStoreAsigurareMasina);
        } else {
            $fileNameToStoreComodat = 'noimage.jpg';
        } 

        if ($request->hasFile('certificat_inmatriculare')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('certificat_inmatriculare')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('certificat_inmatriculare')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCertificatInmatriculare = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('certificat_inmatriculare')->storeAs('public/dcsmm', $fileNameToStoreCertificatInmatriculare);
        } else {
            $fileNameToStoreComodat = 'noimage.jpg';
        }

        if ($request->hasFile('contract_de_comodat')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('contract_de_comodat')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('contract_de_comodat')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreComodat = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('contract_de_comodat')->storeAs('public/dcsmm', $fileNameToStoreComodat);
        } else {
            $fileNameToStoreComodat = 'noimage.jpg';
        }
        
            // Create Post
                $documentmasina = new Documentemasina;
                $documentmasina->buletin_proprietar_masina = $fileNameToStoreBuletinProprietar;
                $documentmasina->asigurare_masina = $fileNameToStoreAsigurareMasina;
                $documentmasina->certificat_inmatriculare = $fileNameToStoreCertificatInmatriculare;
                $documentmasina->contract_de_comodat = $fileNameToStoreComodat;
                $documentmasina->user_id = auth()->user()->id;
                $documentmasina->save();
                return redirect("/documente/create")->with('success', 'Documente introduse');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           // SHOW ARATA PAGINA INDIVIDUALA A POSTULUI cu documente - TINE MINTE ASTA NEBUNULE
           $documentmasina = Documentemasina::find($id);
           return view('documentemasina.show')->with('documentmasina', $documentmasina);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documentmasina = Documentemasina::find($id);
        //Check for correct user
        if (auth()->user()->id !== $documentmasina->user_id) {
            return redirect('/documentemasina')->with('error', 'Unauthorized page');
        }
        return view('documentemasina.edit')->with('documentmasina', $documentmasina);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'buletin_proprietar_masina' => 'image|nullable|max:1999',
            'asigurare_masina'          => 'image|nullable|max:1999',
            'certificat_inmatriculare'  => 'image|nullable|max:1999',
            'contract_de_comodat'       => 'image|nullable|max:1999'
        ]);
               // Handle file upload
        if ($request->hasFile('buletin_proprietar_masina')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('buletin_proprietar_masina')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('buletin_proprietar_masina')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreBuletinProprietar = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('buletin_proprietar_masina')->storeAs('public/dcsm', $fileNameToStoreBuletinProprietar);
        }
        
        if ($request->hasFile('asigurare_masina')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('asigurare_masina')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('asigurare_masina')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreAsigurareMasina = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('asigurare_masina')->storeAs('public/dcsm', $fileNameToStoreAsigurareMasina);
        } 

        if ($request->hasFile('certificat_inmatriculare')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('certificat_inmatriculare')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('certificat_inmatriculare')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCertificatInmatriculare = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('certificat_inmatriculare')->storeAs('public/dcsm', $fileNameToStoreCertificatInmatriculare);
        }

        if ($request->hasFile('contract_de_comodat')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('contract_de_comodat')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('contract_de_comodat')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreComodat = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('contract_de_comodat')->storeAs('public/dcsm', $fileNameToStoreComodat);
        }

            // Update Post
                //$document = new Document;
/*                 $documentmasina = Documentemasina::find($id);
                if ($request->hasFile('buletin_proprietar_masina')) {
                     $documentmasina->buletin_proprietar_masina = $fileNameToStoreBuletinProprietar;
                }
                if ($request->hasFile('asigurare_masina')) {
                  $documentmasina->asigurare_masina = $fileNameToStoreAsigurareMasina;
                }
                if ($request->hasFile('certificat_inmatriculare')) {
                    $documentmasina->certificat_inmatriculare = $fileNameToStoreCertificatInmatriculare;
                }
                if ($request->hasFile('contract_de_comodat')) {
                    $documentmasina->contract_de_comodat = $fileNameToStoreComodat;
                } */

                $documentmasina = Documentemasina::find($id);
                if ($request->hasFile('buletin_proprietar_masina')) {
                     $documentmasina->buletin_proprietar_masina = $fileNameToStoreBuletinProprietar;
                }
                if ($request->hasFile('asigurare_masina')) {
                  $documentmasina->asigurare_masina = $fileNameToStoreAsigurareMasina;
                }
                if ($request->hasFile('certificat_inmatriculare')) {
                    $documentmasina->certificat_inmatriculare = $fileNameToStoreCertificatInmatriculare;
                }
                if ($request->hasFile('contract_de_comodat')) {
                    $documentmasina->contract_de_comodat = $fileNameToStoreComodat;
                }

                $documentmasina->save();
                return redirect("/documentemasina/$documentmasina->id/edit")->with('success', 'Document updatad');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             $documentmasina = Documentemasina::find($id);
             //Check for correct user
             if (auth()->user()->id !== $documentmasina->user_id) {
                return redirect('/documentemasina')->with('error', 'Unauthorized page');
             }
             $documentmasina->delete(); 
             return redirect("/documentemasina")->with('success', 'Documente masina deleted');  
    }
}
