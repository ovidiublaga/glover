<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home() {
        return view('pages/home')->with(
            [
            'title' => 'Welcome to Laravel!!!!!!!'
            ]
        );
    }


    public function index() {
        $title = 'Welcome to Laravel!!!!!!!';
        return view('pages.index')->with('title', $title);
    }
    public function about() {
        
        return view('pages.despre-glovo');
    }

    public function devinoGlover() {
        
        return view('pages.devino-glover');
    }
    public function despreNoi() {
        
        return view('pages.despre-noi');
    }

    public function contact() {
        
        return view('pages.contact');
    }

    public function services() {
        $data = array(
            'title' => 'Services',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}
