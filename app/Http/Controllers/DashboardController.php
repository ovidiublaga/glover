<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Document;
use DB;
use Storage;
use \Carbon\Carbon;
use \App\Hcl;



class DashboardController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function administrare() {
        return view('administrare/administrare')->with(
            [
            'title' => 'Esti pe pagina de administrare si asta e titlul din DashboardController@administrare'
            ]
        );
    }

    public function iban() {
        return view('administrare/iban')->with(
            [
            'title' => 'IBAN'
            ]
        );
    }

    public function schimbaIban(Request $request){
        $user = User::find(auth()->user()->id);
        $user->iban = json_encode(
            [
                'status' => 0,
                'iban' => $request->input('iban')
            ]    
        );
        $user->save();
        return back();
    }
        // DE AICI A BAGAT OVIDIU FUNCTII
        public function protectiamuncii() {
            return view('administrare.protectiamuncii');
        }


        public function moddelivrare() {
            return view('administrare.moddelivrare');
        }

        public function contractdemunca() {
            return view('administrare.contractdemunca');
        }

        public function editeazainformatiile() {
            return view('administrare.editeazainformatiile');
        }
        public function solicitaadeverintasalariat() {
            return view('administrare.solicitaadeverintasalariat');
        }
        public function actiunicartedemunca() {
            return view('administrare.actiunicartedemunca');
        }

        public function solicitadeciziadeincetare() {
            return view('administrare.solicitadeciziadeincetare');    
        }

    public function documentelemele(){
        $xx = Document::where(
            [
                'user_id' => auth()->user()->id
            ]
        )
        ->orderBy('updated_at', 'asc')
        ->get();
        $documents = [];
        foreach($xx as $document){
            $documents[$document->type_id]['url'] = $document->url;
            $documents[$document->type_id]['user_id'] = $document->user_id;
            $documents[$document->type_id]['expire'] = $document->expire;
            $documents[$document->type_id]['status_id'] = $document->status_id;
            $documents[$document->type_id]['updated_at'] = $document->updated_at;
        }
        $document_types = DB::table('document_types')->get();
     //  return $documents;
        return view('administrare/documentelemele')->with(
            [
                'title' => 'documentelemele',
                'document_types' => $document_types,
                'documents' => $documents
            ]
        );
    }

    public function incarcaDocument(Request $request){
        Storage::makeDirectory('public/utilizatori/' . auth()->user()->id. '/', 0755, true);
       // Storage::makeDirectory('xxx', 0755, true);
       //Storage::makeDirectory('public/utilizatori/' . auth()->user()->id. '/', 0755, true);
       // Storage::makeDirectory('public/utilizatori/' . auth()->user()->id. '/', 0755, true);
       $validatedData = $request->validate([
        'county_id' => 'required',
        'name' => 'required',
        'region' => 'required'
          ]);
        $data = $request->validate([

                    'files' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

         ]);


        if($request->hasfile('files'))
            {
                foreach($request->file('files') as $file)
                {
                    $name= time() . $file->getClientOriginalName();
                    //routa pentru LINUX
                    $file->move( '/var/www/devinoglover/storage/app/public/utilizatori/' . auth()->user()->id. '/', $name);  
                    // ROUTA PENTRU WINDOWS
                    //$file->move( '/xampp/htdocs/devinoglover/storage/app/public/utilizatori/' . auth()->user()->id. '/', $name);  
                    $data[] = $name;  
                }
            }
        $document = new Document;
        $document->user_id = auth()->user()->id;
        $document->type_id = $request->input("document_type");
        $document->expire = $request->input("expire");
        $document->status_id = 0;
        $document->url = json_encode($data);
        $document->save();
        return back();
    }

    public function secureFile($user_id, $file){
        if($user_id == auth()->user()->id || auth()->user()->role_id == 0){
            $file =  Storage::get('public/utilizatori/'.$user_id.'/' .$file);
            return response($file, 200)->header('Content-Type', 'image/jpeg');
        } else {
            return "Acces interzis";
        }
        
     
    }

    public function test(){

        $hcls = DB::table('hcl_fisiere')
                ->join('hcl', 'hcl.id_hcl', '=', 'hcl_fisiere.id_hcl')
                ->select('*')
                ->orderBy('hcl.data', 'desc')
                ->get();
       // return $hcls;
        $content = "";
        foreach($hcls as $hcl){
            $content .= "<a target='_blank' href='http://comunapodoleni.classgrup.ro/fisiere/".$hcl->url_fisier."'>Hotararea nr ".$hcl->numar." / " . $hcl->data . " ". $hcl->descriere . "</a> ";
        }
        return  $content;
    }

}
