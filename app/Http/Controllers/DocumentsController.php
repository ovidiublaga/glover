<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Document;

class DocumentsController extends Controller
{
            /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('documente.index');
        //$documents = Document::orderBy('created_at', 'desc')->paginate(5);
        //$documents = Document::orderBy('created_at', 'desc');
       // $documents = Document::all();
        $documents = Document::orderBy('created_at', 'desc')->get();
        return view('documente.index')->with('documents', $documents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documente.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            //'title'       => 'required',
            //'body'        => 'required',
            // 'carte_de_identitate' => ['required', 'image|max:1999'],
            // 'adeverinta_de_medic' => ['required', 'image|max:1999'],
            // 'diploma' => ['required', 'image|max:1999'],
            // 'cazier_judiciar' => 'image|nullable|max:1999'
            'carte_de_identitate' => 'required|image|max:1999',
            'adeverinta_de_medic' => 'required|image|max:1999',
            'diploma'             => 'required|image|max:1999',
            'cazier_judiciar'     => 'image|nullable|max:1999'
        ]);

               // Handle file upload
        if ($request->hasFile('carte_de_identitate')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('carte_de_identitate')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('carte_de_identitate')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCarteIdentitate = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('carte_de_identitate')->storeAs('public/dcs', $fileNameToStoreCarteIdentitate);
        }
        
        if ($request->hasFile('adeverinta_de_medic')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('adeverinta_de_medic')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('adeverinta_de_medic')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreAdeverintaMedic = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('adeverinta_de_medic')->storeAs('public/dcs', $fileNameToStoreAdeverintaMedic);
        } 

        if ($request->hasFile('diploma')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('diploma')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('diploma')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreDiploma = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('diploma')->storeAs('public/dcs', $fileNameToStoreDiploma);
        }

        if ($request->hasFile('cazier_judiciar')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('cazier_judiciar')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('cazier_judiciar')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCazier = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('cazier_judiciar')->storeAs('public/dcs', $fileNameToStoreCazier);
        } else {
            $fileNameToStoreCazier = 'noimage.jpg';
        }

            // Create Post
                $document = new Document;
                $document->carte_de_identitate = $fileNameToStoreCarteIdentitate;
                $document->adeverinta_de_medic = $fileNameToStoreAdeverintaMedic;
                $document->diploma = $fileNameToStoreDiploma;
                $document->cazier_judiciar = $fileNameToStoreCazier;
                $document->user_id = auth()->user()->id;
                $document->save();
                return redirect("/documente/create")->with('success', 'Documente introduse');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // SHOW ARATA PAGINA INDIVIDUALA A POSTULUI cu documente - TINE MINTE ASTA NEBUNULE
        $document = Document::find($id);
        return view('documente.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = Document::find($id);
        //Check for correct user
        if (auth()->user()->id !== $document->user_id) {
            return redirect('/documente')->with('error', 'Unauthorized page');
        }
        return view('documente.edit')->with('document', $document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'carte_de_identitate' => 'image|max:1999',
            'adeverinta_de_medic' => 'image|max:1999',
            'diploma'             => 'image|max:1999',
            'cazier_judiciar'     => 'image|nullable|max:1999'
        ]);

               // Handle file upload
        if ($request->hasFile('carte_de_identitate')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('carte_de_identitate')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('carte_de_identitate')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCarteIdentitate = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('carte_de_identitate')->storeAs('public/dcs', $fileNameToStoreCarteIdentitate);
        }
        
        if ($request->hasFile('adeverinta_de_medic')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('adeverinta_de_medic')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('adeverinta_de_medic')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreAdeverintaMedic = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('adeverinta_de_medic')->storeAs('public/dcs', $fileNameToStoreAdeverintaMedic);
        } 

        if ($request->hasFile('diploma')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('diploma')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('diploma')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreDiploma = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('diploma')->storeAs('public/dcs', $fileNameToStoreDiploma);
        }

        if ($request->hasFile('cazier_judiciar')) {
            // GET filename with the extension
            $filenameWithExt = $request->file('cazier_judiciar')->getClientOriginalName();
            // GET just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //GET just extension
            $extension = $request->file('cazier_judiciar')->getClientOriginalExtension();
            //Filename to store
            $fileNameToStoreCazier = $filename.'_'.time().'.'.$extension;
            //UPLOAD IMAGE
            $path = $request->file('cazier_judiciar')->storeAs('public/dcs', $fileNameToStoreCazier);
        }

            // Update Post
                //$document = new Document;
                $document = Document::find($id);
                if ($request->hasFile('carte_de_identitate')) {
                     $document->carte_de_identitate = $fileNameToStoreCarteIdentitate;
                }
                if ($request->hasFile('adeverinta_de_medic')) {
                  $document->adeverinta_de_medic = $fileNameToStoreAdeverintaMedic;
                }
                if ($request->hasFile('diploma')) {
                    $document->diploma = $fileNameToStoreDiploma;
                }
                if ($request->hasFile('cazier_judiciar')) {
                    $document->cazier_judiciar = $fileNameToStoreCazier;
                }
                $document->save();
                return redirect("/documente/$document->id/edit")->with('success', 'Document updatad');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
             $document = Document::find($id);
             //Check for correct user
             if (auth()->user()->id !== $document->user_id) {
                return redirect('/documente')->with('error', 'Unauthorized page');
             }
             $document->delete(); 
             return redirect("/documente")->with('success', 'Documente deleted');  
    }
}
