<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Atributul :attribute must be accepted.',
    'active_url' => 'Atributul :attribute is not a valid URL.',
    'after' => 'Atributul :attribute must be a date after :date.',
    'after_or_equal' => 'Atributul :attribute must be a date after or equal to :date.',
    'alpha' => 'Atributul :attribute may only contain letters.',
    'alpha_dash' => 'Atributul :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'Atributul :attribute may only contain letters and numbers.',
    'array' => 'Atributul :attribute must be an array.',
    'before' => 'Atributul :attribute must be a date before :date.',
    'before_or_equal' => 'Atributul :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'Atributul :attribute must be between :min and :max.',
        'file' => 'Atributul :attribute must be between :min and :max kilobytes.',
        'string' => 'Atributul :attribute must be between :min and :max characters.',
        'array' => 'Atributul :attribute must have between :min and :max items.',
    ],
    'boolean' => 'Atributul :attribute field must be true or false.',
    'confirmed' => 'Atributul :attribute confirmation does not match.',
    'date' => 'Atributul :attribute is not a valid date.',
    'date_equals' => 'Atributul :attribute must be a date equal to :date.',
    'date_format' => 'Atributul :attribute does not match the format :format.',
    'different' => 'Atributul :attribute and :other must be different.',
    'digits' => 'Atributul :attribute must be :digits digits.',
    'digits_between' => 'Atributul :attribute must be between :min and :max digits.',
    'dimensions' => 'Atributul :attribute has invalid image dimensions.',
    'distinct' => 'Atributul :attribute field has a duplicate value.',
    'email' => 'Atributul :attribute must be a valid email address.',
    'ends_with' => 'Atributul :attribute must end with one of the following: :values.',
    'exists' => 'Atributul selected :attribute is invalid.',
    'file' => 'Atributul :attribute must be a file.',
    'filled' => 'Atributul :attribute field must have a value.',
    'gt' => [
        'numeric' => 'Atributul :attribute must be greater than :value.',
        'file' => 'Atributul :attribute must be greater than :value kilobytes.',
        'string' => 'Atributul :attribute must be greater than :value characters.',
        'array' => 'Atributul :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'Atributul :attribute must be greater than or equal :value.',
        'file' => 'Atributul :attribute must be greater than or equal :value kilobytes.',
        'string' => 'Atributul :attribute must be greater than or equal :value characters.',
        'array' => 'Atributul :attribute must have :value items or more.',
    ],
    'image' => 'Atributul :attribute must be an image.',
    'in' => 'Atributul selected :attribute is invalid.',
    'in_array' => 'Atributul :attribute field does not exist in :other.',
    'integer' => 'Atributul :attribute must be an integer.',
    'ip' => 'Atributul :attribute must be a valid IP address.',
    'ipv4' => 'Atributul :attribute must be a valid IPv4 address.',
    'ipv6' => 'Atributul :attribute must be a valid IPv6 address.',
    'json' => 'Atributul :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'Atributul :attribute must be less than :value.',
        'file' => 'Atributul :attribute must be less than :value kilobytes.',
        'string' => 'Atributul :attribute must be less than :value characters.',
        'array' => 'Atributul :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'Atributul :attribute must be less than or equal :value.',
        'file' => 'Atributul :attribute must be less than or equal :value kilobytes.',
        'string' => 'Atributul :attribute must be less than or equal :value characters.',
        'array' => 'Atributul :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'Atributul :attribute may not be greater than :max.',
        'file' => 'Atributul :attribute may not be greater than :max kilobytes.',
        'string' => 'Atributul :attribute may not be greater than :max characters.',
        'array' => 'Atributul :attribute may not have more than :max items.',
    ],
    'mimes' => 'Atributul :attribute must be a file of type: :values.',
    'mimetypes' => 'Atributul :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'Atributul :attribute must be at least :min.',
        'file' => 'Atributul :attribute must be at least :min kilobytes.',
        'string' => 'Atributul :attribute must be at least :min characters.',
        'array' => 'Atributul :attribute must have at least :min items.',
    ],
    'not_in' => 'Atributul selected :attribute is invalid.',
    'not_regex' => 'Atributul :attribute format is invalid.',
    'numeric' => 'Atributul :attribute must be a number.',
    'password' => 'Atributul password is incorrect.',
    'present' => 'Atributul :attribute field must be present.',
    'regex' => 'Atributul :attribute format is invalid.',
    'required' => 'Atributul :attribute este obligatoriu.',
    'required_if' => 'Atributul :attribute este obligatoriu when :other is :value.',
    'required_unless' => 'Atributul :attribute este obligatoriu unless :other is in :values.',
    'required_with' => 'Atributul :attribute este obligatoriu when :values is present.',
    'required_with_all' => 'Atributul :attribute este obligatoriu when :values are present.',
    'required_without' => 'Atributul :attribute este obligatoriu when :values is not present.',
    'required_without_all' => 'Atributul :attribute este obligatoriu when none of :values are present.',
    'same' => 'Atributul :attribute and :other must match.',
    'size' => [
        'numeric' => 'Atributul :attribute must be :size.',
        'file' => 'Atributul :attribute must be :size kilobytes.',
        'string' => 'Atributul :attribute must be :size characters.',
        'array' => 'Atributul :attribute must contain :size items.',
    ],
    'starts_with' => 'Atributul :attribute must start with one of the following: :values.',
    'string' => 'Atributul :attribute must be a string.',
    'timezone' => 'Atributul :attribute must be a valid zone.',
    'unique' => 'Atributul :attribute has already been taken.',
    'uploaded' => 'Atributul :attribute failed to upload.',
    'url' => 'Atributul :attribute format is invalid.',
    'uuid' => 'Atributul :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
