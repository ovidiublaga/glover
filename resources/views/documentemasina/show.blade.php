@extends('layouts.app')
@section('content')
    <a href="/documentemasina">Go back</a>
     
<table>
    <tr>
        <td><img src="/storage/dcsm/{{$documentmasina->buletin_proprietar_masina}}" style="width: 70%"></td>
        <td><img src="/storage/dcsm/{{$documentmasina->asigurare_masina}}" style="width: 70%"></td>
        <td><img src="/storage/dcsm/{{$documentmasina->certificat_inmatriculare}}" style="width: 70%"></td>
        <td><img src="/storage/dcsm/{{$documentmasina->contract_de_comodat}}" style="width: 70%"></td>
    </tr>
</table>
    <hr>
    <small>Written on {{$documentmasina->created_at}} by {{$documentmasina->user->name}}</small>

    @if(!Auth::guest())
        @if(Auth::user()->id == $documentmasina->user_id)
            <a href="/documentemasina/{{$documentmasina->id}}/edit">Edit</a>
            {!! Form::open(['action' => ['DocumentemasinaController@destroy', $documentmasina->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => "btn btn-danger"])}}
            {!! Form::close() !!}
        @endif
    @endif
    
@endsection