@extends('layouts.app')
@section('content')
    <h1>Introduceti documente masina</h1>
    {!! Form::open(['action' => 'DocumentemasinaController@store', 'method' => 'POST',  'enctype' => 'multipart/form-data']) !!}
    {{-- <div class="form-group">
    	{{Form::label('title', 'Title')}}
    	{{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div> --}}

    <hr>
    <div class="form-group">
        <label>Buletin proprietar masina (Optional)</label><br>
        {{Form::file('buletin_proprietar_masina')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Asigurare masina (Optional)</label><br>
        {{Form::file('asigurare_masina')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Certificat inmatriculare (Optional)</label><br>
        {{Form::file('certificat_inmatriculare')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Contract de comodat   (Optional)</label><br>
        {{Form::file('contract_de_comodat')}}
    </div>   
    <hr>


    {{Form::submit('Submit', ['class' => "btn btn-primary"])}}
    {!! Form::close() !!}
@endsection