@extends('layouts.app')
@section('content')
    <h1>Documente masina</h1>
    @if(count($documentemasina) > 0)
        @foreach ($documentemasina as $documentmasina)
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcsm/{{$documentmasina->buletin_proprietar_masina}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcsm/{{$documentmasina->asigurare_masina}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcsm/{{$documentmasina->certificat_inmatriculare}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcsm/{{$documentmasina->contract_de_comodat}}" style="width: 70%">
            </div>
        </div>
            <div>
                <a href="documentemasina/{{$documentmasina->id}}">Vezi situatia</a>
            
            <small>Written on {{$documentmasina->created_at}} </small>
            <small>Scris by {{$documentmasina->user->name}}</small>
            </div>
        @endforeach
        {{-- {{$documents->links()}} --}}
    @else
        <p>Nniciun document gasit</p>
    @endif
@endsection