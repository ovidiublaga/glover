@extends('layouts.app')
@section('content')
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['DocumentemasinaController@update', $documentmasina->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    
    <div class="form-group">
        <img src="/storage/dcsm/{{$documentmasina->buletin_proprietar_masina}}" style="width: 10%">
        {{Form::file('buletin_proprietar_masina')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcsm/{{$documentmasina->asigurare_masina}}" style="width: 10%">
        {{Form::file('asigurare_masina')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcsm/{{$documentmasina->certificat_inmatriculare}}" style="width: 10%; height: 2% !important;">
        {{Form::file('certificat_inmatriculare')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcsm/{{$documentmasina->contract_de_comodat}}" style="width: 10%">
        {{Form::file('contract_de_comodat')}}
    </div>

    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => "btn btn-primary"])}}
    {!! Form::close() !!}
@endsection