
<ul class="main-menu" onmouseover="desfasoaraMeniu(this)">
    <li class="close_menu" onclick="inchideMeniu()">
        <i class="fas fa-times" onclick="inchideMeniu()"></i>
        <span>Inchide meniu</span
    </li>
    <li>
        <a href="/register">
            <i class="fas fa-home"></i>
            <span>Creare cont</span
        </a>
    </li>
    <li>
        <a href="/login">
            <i class="fas fa-home"></i>
            <span>Login</span
        </a>
    </li>
    <li>
        <a href="/">
            <i class="fas fa-home"></i>
            <span>Acasa</span
        </a>
    </li>
    <li>
        <a href="/despre-glovo">
            <i class="fas fa-info"></i>
            <span>Despre Glovo</span
        </a>
    </li>
    <li>
        <a href="/devino-glover">
            <i class="fas fa-user-check"></i>
            <span>Devino Glover</span
        </a>
    </li>
    <li>
        <a href="/despre-noi">
            <i class="fas fa-info"></i>
            <span>Despre noi</span
        </a>
    </li>
    @auth
        <!--Acestea apar doar cand utilizatorul este logat -->
        <li>
            <a href="/administrare/documentele-mele">
                <i class="far fa-folder-open"></i>
                <span>Documente</span
            </a>
        </li>
        <li>
            <a href="/contractdemunca">
                <i class="far fa-file-alt"></i>
                <span>Contract de munca</span
            </a>
        </li>
        <li>
            <a href="/protectiamuncii">
                <i class="fas fa-wrench"></i>
                <span>Protectia muncii</span
            </a>
        </li>
        <li>
            <a href="/moddelivrare">
               <i class="fas fa-biking"></i>
                <span>Mod de livrare</span
            </a>
        </li>
        <li>
            <a href="/administrare/iban">
                <i class="fas fa-credit-card"></i>
                <span>IBAN</span
            </a>
        </li>
        <li>
            <a href="/editeazainformatiile">
                <i class="far fa-edit"></i>
                <span>Editeaza informatiile</span
            </a>
        </li>
        <li>
            <a href="/solicitaadeverintasalariat">
                <i class="far fa-file-alt"></i>
                <span>Solicita adeverinta salariat</span
            </a>
        </li>
        <li>
            <a href="/actiunicartedemunca">
                <i class="far fa-pause-circle"></i>
                <span>Solicita suspendare / <br /> reactivare carte de munca</span
            </a>
        </li>
        <li>
            <a href="/solicitadeciziadeincetare">
                <i class="far fa-stop-circle"></i>
                <span>Solicita decizie de incetare</span
            </a>
        </li>
    @endauth
      
        <li>
            <a href="/contact">
                <i class="fas fa-mail-bulk"></i>
                <span>Contact</span
            </a>
        </li>
    @auth
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i>
                <span>Deuatentificare</span
            </a>
        </li> 
    @endauth
        
    
</ul>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>


