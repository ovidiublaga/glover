@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
    {{-- <div class="container mt-3">
       DESPRE GLOVO
    </div> --}}
    <div style="text-align: center; padding-top: 30px !important;">
        <h2>Despre noi</h2>
    </div>
    <!-- Home cards 1 -->
    <section class="home-cards-desprenoi">
      <div class="maxima">
        <img src="img/logo-Dakota.png" alt="" class="centerImagine imagineDesprenoi">
      </div>
      <div>
        <p>  
            SC DAKOTA GENERAL SRL este o firma partenera oficiala GLOVO in toate orasele din tara care ofera posibilitatea curierilor sa lucreze in aplicatia GLOVO respectand toate normele legale de angajare.
        </p>
        <p>Dakota General iti ofera posibilitatea sa ai o evidenta clara asupra situatiei contractuale.</p>
      </div>
      
     
    </section>
@endsection
