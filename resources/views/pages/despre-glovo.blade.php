@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
    {{-- <div class="container mt-3">
       DESPRE GLOVO
    </div> --}}

    <!-- Home cards 1 -->
    <section class="home-cards">
      <div>
        <img src="img/esti-propriul-tau-sef.png" alt="" class="centerImagine">
        <h3>Esti propriul tau sef</h3>
        <p>
          Glovo iti ofera oportunitatea de a lucra dupa propriul program si de a castiga bani, <span>fara sefi</span>.
        </p>
      </div>
      <div>
        <img src="img/cati-bani-faci.png" alt=""  class="centerImagine"/>
        <h3>Cati bani faci?</h3>
        <p>
          Curierii Glovo pot castiga pana la <span>25 lei pe ora</span>. Cat castigi depinde de experienta ta si de evaluarile clientilor si ale partenerilor Glovo.
        </p>
      </div>
      <div>
        <img src="img/lucreaza oricand doresti.png" alt=""  class="centerImagine"/>
        <h3>Lucreaza oricand doresti</h3>
        <p>
         Rotunjeste-ti veniturile si lucreaza dupa <span>propriul program</span>. Cu Glovo ai libertatea de a alege cand sa lucrezi.
        </p>
      </div>
      <div>
        <img src="img/de-ce-ai-nevoie.png" alt=""  class="centerImagine"/>
        <h3>De ce ai nevoie?</h3>
        <p>
          Un zambet larg, <span>un mijloc de transport, un telefon</span> cu Android sau iOS si <span>minimum 18 ani</span>.
        </p>
      </div>
    </section>
@endsection
