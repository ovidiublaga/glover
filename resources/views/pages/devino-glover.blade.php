@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
    {{-- <div class="container mt-3">
       DESPRE GLOVO
    </div> --}}
    <div style="text-align: center; padding-top: 30px !important;">
        <h2>Devino Glover</h2>
    </div>
    <!-- Home cards 1 -->
    <section class="home-cards-devino">
      <div>
        <img src="img/pasul-1.png" alt="" class="centerImagine imagineDevino">
        <h3>Creaza-ti un cont</h3>
        <p>
          <span>Creaza-ti un cont</span>.
        </p>
      </div>
      <div>
        <img src="img/pasul-2.png" alt=""  class="centerImagine imagineDevino"/>
        <h3>Pasul 2</h3>
        <p>
          <span>Adauga actele necesare</span>
        </p>
      </div>
      <div>
        <img src="img/pasul-3.png" alt=""  class="centerImagine imagineDevino"/>
        <h3>Pasul 3</h3>
        <p>
         <span>Descarca contractul de munca</span>
        </p>
      </div>
     
    </section>
    <div style="text-align: center; padding-top: 30px !important;">
        <h6 style="text-align: center;">Programeaza-te la o sesiune de informare pentru a putea primi echipamenul<br> <span style="color: #33a384">glovoapp.com/ro/glovers</span></h6>
    </div>
@endsection
