<!DOCTYPE html>
<html>
<head>
    <title>devino Glover</title>
    <meta charset="UTF-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <div class="containerHome">
        <header class="showcase">
         <div class="layertop ">
            <nav class="main-nav">
              <div class="iconbar paddingIcon">
                <i class="fas fa-bars fa-2x"></i>
              </div>
              <div>
                <a href="/">
                  <img src="img/devinoGlover.ro.png" alt="Devino Glover" class="logo paddingImagine" >
                </a>
              </div>
                @include('inc/meniu_stanga')
              <ul class="right-menu">
                @auth
                    <a href="/home" class="contfirst">
                    Contul Meu
                    </a> 
                @else
                    <a href="/login" class="contfirst">
                      Autentificare
                    </a> 
                @endauth
              </ul>
            </nav>
          </div>
          <div class="layer">
              <h2>Mărește-ți <span>veniturile</span> cu</h2>
              <h2>cu Glovo</h2>
              </p>
              <a href="/devino-glover" class="btn">
              Află pașii <i class="fas fa-chevron-right"></i>
              </a>
            </div>
        </header>
        <footer class="footer-new">
          <div class="footer-inner">
               <img src="/img/devinoGlover.ro.png" alt="Devino Glover" >
                  <ul style="padding-left: 35px;">
                     <li><i class="fas fa-mobile-alt"></i> 0748389137</li>
                      <li><i class="fas fa-mobile-alt"></i> 0743609446</li>
                      <li><i class="fas fa-mail-bulk"></i> dakotagenerale@gmail.com</li>
                      <li><i class="fab fa-whatsapp"></i> Dakota General</li>
                      <li>Copyright &copy; Dakota General. Toate drepturile rezervate</li>
                  <ul> 
          </div>
        </footer> 
      </div>
     
    <script src="js/app.js"></script>
</body>

</html>