@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
           <div class="card text-center card-form">
            <div class="card-body">
              <h3>Solicitare adeverinta salariat <i class="fas fa-file-contract"></i></h3>
              <p>Va rugam sa specificati motivul pentru emiterea adeverintei.</p>
              <form>

                <div class="form-group">
                  <textarea class="form-control form-control-lg" placeholder="Va rugam sa specificati motivul pentru emiterea adeverintei. *" rows="7" ></textarea>
                </div>

               
                <input type="submit" value="Trimite" class="btn btn-outline-light btn-block">
              </form>
            </div>
          </div>
    </div>
</div>

@endsection
