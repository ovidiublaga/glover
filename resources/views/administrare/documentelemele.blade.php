@extends('layouts/main')
@section('footer_scripts')
<script>
$(document).ready(function(){
	//toggle the component with class accordion_body
	$(".accordion_head").click(function(){
		if ($('.accordion_body').is(':visible')) {
			$(".accordion_body").slideUp(300);
			$(".plusminus").text('+');
		}
        if( $(this).next(".accordion_body").is(':visible')){
            $(this).next(".accordion_body").slideUp(300);
            $(this).children(".plusminus").text('+');
        }else {
            $(this).next(".accordion_body").slideDown(300); 
            $(this).children(".plusminus").text('-');
        }
	});
});
</script>
@endsection
@section('content')
<div class="">
    <div class="container mt-3">
        <div class="accordion_container">
          @foreach($document_types as $document_type)
          <form action="/administrare/documentele-mele/incarcaDocument" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="accordion_head">
             {{$document_type->title}}
              @if(isset($documents[$document_type->id]['status_id']))
                @switch($documents[$document_type->id]['status_id'])
                  @case(0)
                    <span class="badge badge-warning">In curs de revizuire</span>
                    @break
                  @case(1)
                    <span class="badge badge-success">Success</span>
                    @break
                  @case(2)
                    <span class="badge badge-danger">Invalid</span>
                    @break
                  @default  
                    Status inexistent
                @endswitch
              @else
                <span class="badge badge-secondary">Document neincarcat</span>
              @endif
              @if(isset($documents[$document_type->id]['updated_at']))
                  <span class="badge badge-light">{{$documents[$document_type->id]['updated_at']}}</span>
              @endif
              @if($document_type->optional == 1)
                <span class="optional">
                  (optional)
                </span>
              @endif
              <span class="plusminus">+</span>
            </div>
              <div class="accordion_body" style="display: none;"> 
                <div class="container">
                  @if(isset($documents[$document_type->id]['status_id']) && $documents[$document_type->id]['status_id'] !== 1 && $document_type->expire !== 0)
                    @if($document_type->expire == 1)
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="expire">Data expirarii</label>
                          </div>
                        </div>
                        <div class="col-8">
                          <div class="form-group">
                            <input type="date" name="expire" class="form-control" id="expire" placeholder="" required>
                          </div>
                        </div>
                      </div>
                    @endif
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="files">Incarca documentele</label>
                          </div>
                        </div>
                        <div class="col-8">
                          <div class="form-group">
                            <input type="file" name="files[]" class="form-control" id="files" placeholder="" multiple required accept=".jpg,.jpeg,.png">
                          </div>
                        </div>
                      </div>
                      <br />
                      <input type="hidden" name="document_type" value="{{$document_type->id}}">
                      <input type="submit" name="ss" value="Trimite" class="btn btn-xs btn-success" placeholder="">
                      <hr />
                    @if(isset($documents[$document_type->id]['url']))
                      @foreach(json_decode($documents[$document_type->id]['url']) as $url)
                        <a target="_blank" style="text-decoration:underline; color:blue;" href="/documente/utilizatori/{{auth()->user()->id}}/{{$url}}">{{$url}}</a> <br />
                      @endforeach
                    @endif
                  @endif
                  @if(isset($documents[$document_type->id]['expire']))
                 
                Data expirarii: {{$documents[$document_type->id]['expire']}}
                @endif
              </div>
              </div>
          </form>
          @endforeach
        </div>
    </div>
</div>
<style>
.optional {
  color: #fcdb95;
}
.accordion_container {
    border: 1px solid #3bbb9b;
    cursor: pointer;
}
.accordion_container .container {
  padding: 20px 20px 10px 20px;
}
.accordion_head {
    background-color: #3bbb9b;
    color: white;
    cursor: pointer;
    font-family: arial;
    font-size: 15px;
    margin: 0 0 1px 0;
    padding: 15px 15px;
    font-weight: bold;
}
.accordion_body p{
    padding: 15px;
    margin: 0px;
    color: #3bbb9b;
}
.plusminus{
	  float:right;
}
</style>
@endsection
