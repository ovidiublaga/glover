@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
<div class="container mt-3">
    <h2>Protectia muncii <i class="fas fa-file-medical"></i></h2>
    <br>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" href="#home">Documente primite</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#menu1">Documente semnate</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#menu2">Documente respinse</a>
      </li>
    </ul>
  
    <!-- Tab panes -->
    <div class="tab-content">
      <div id="home" class="container tab-pane active"><br>
        <h6 class="pb-2">Documentele primite pentru protectia muncii</h6>
        <div class="card text-left card-form">
          <div class="card-body">
           <div class="d-flex flex-row">
               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
               </div>
               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">12.09.2020 <i class="far fa-file-alt"></i></button>
               </div>


               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">15.10.2020 <i class="far fa-file-alt"></i></button>
               </div>

               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">16.11.2020 <i class="far fa-file-alt"></i></button>
               </div>
            </div>
            <h4>Cu click pe ele se downloadeaza</h4>
          </div>
        </div>
      </div>
      <div id="menu1" class="container tab-pane fade"><br>
        
        <h6 class="pb-2">Documente semnate</h6>
        <div class="card text-left card-form">
          <div class="card-body">
           <div class="d-flex flex-row">
               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
               </div>
               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">12.09.2020 <i class="far fa-file-alt"></i></button>
               </div>

               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">16.11.2020 <i class="far fa-file-alt"></i></button>
               </div>
            </div>
            <h4>Nu se mai downloadeaza nimica cu click, este doar de afisare bag picioarele</h4>
          </div>
        </div>
      <!--    
        <div class="d-flex flex-row">
          <div class="p-4 align-self-start">
            <p data-toggle="modal" data-target="#contactModal" class="mb-0 pointing">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Incidunt, quaerat cupiditate quia voluptate voluptatum repellendus?</p>
          </div>
           <div class="p-4 align-self-end">
            <i class="fa fa-check checkedGreen"></i>
          </div>
        </div> -->



        
                         <!-- CONTACT MODAL SCRIPT -->
<div class="modal fade text-dark" id="contactModal">
<div class="modal-dialog">
  <div class="modal-content"> 
    <div class="modal-body">
      <img src="img/buletin_romania.jpg" style="width: 100%">
    </div>
  </div>
</div>
</div>

<!-- END CONTACT MODAL -->

      </div>
      <div id="menu2" class="container tab-pane fade"><br>
        <h5>Documente respinse</h5>
         <div class="card text-left card-form">
          <div class="card-body">
           <div class="d-flex flex-row">
               <div class="col-md-2">
                      <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
               </div>
               
            </div>
            <h6 class="text-danger pt-3">* Documentele de mai sus nu au fost aprobate de catre administrator.</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
