@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
           <div class="card text-center card-form">
            <div class="card-body">
              <div class="flexgeneral mb-3">
                      <h4>Editare informatii cont</h4>
                      <i class="far fa-edit"></i>
                  </div>
            <h6 class="text-warning-2 pt-3">Informatii sunt in curs de revizuire si vor fi schimbat cat de repede posibil</h6>
              <form>
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" placeholder="Prenume *">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" placeholder="Nume *">
                </div>
                   <div class="form-group">
                  <input type="email" class="form-control form-control-lg cancel-pointer" placeholder="Email *" disabled = "disabled">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" placeholder="Telefon *">
                </div>

                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" placeholder="Parola *">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" placeholder="Confirmati parola*">
                </div>
                <div class="form-group">
                  <label for="sel1">Mod de livrare: </label>
                  <select class="form-control">
                    <option>Masina</option>
                    <option>Bicicleta</option>
                    <option>Scooter</option>
                    <option>Pe jos</option>
                  </select>
              </div>
                <input type="submit" value="Editeaza" class="btn btn-outline-light btn-block">
              </form>
           
              <h2 class="form-text">Campul de email sa fie needitabil</h2>
            </div>
          </div>
    </div>
</div>
@endsection
