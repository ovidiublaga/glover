@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
    {{-- <div class="container mt-3">
        Contract de munca
    </div> --}}
    <div class="container mt-3">
        <h2>Contract de munca <i class="fas fa-file-contract"></i></h2>
        <br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#home">Contract de munca</a>
          </li>


          <li class="nav-item">
              <a class="nav-link" href="#menu-contract">Trimiteti contractul semnat pentru aprobare</a>
          </li>



          <li class="nav-item">
            <a class="nav-link" href="#menu1">Contract de munca semnat / aprobat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#menu2">Contract de munca respins</a>
          </li>
        </ul>
      
        <!-- Tab panes -->
        <div class="tab-content">
          <div id="home" class="container tab-pane active"><br>
            <h6 class="pb-2 text-warning-2">! Ati primit urmatorul contract de munca pentru semnare si aprobare. Va rugam sa il descarcati si sa il retrimiteti semnat <a href="#" class="menu-contract-to-show-first">aici</a>.</h6>
            <div class="card text-left card-form">
              <div class="card-body">
               <div class="d-flex flex-row">
                   <div class="col-md-2">
                          <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
                   </div>
                   
                </div>
                <h4>Cu click pe ele se downloadeaza</h4>
              </div>
            </div>
          </div>
          <div id="menu1" class="container tab-pane fade"><br>
            
            <h6 class="pb-2">Documente semnate si aprobate de catre administrator</h6>
            <div class="card text-left card-form">
              <div class="card-body">
               <div class="d-flex flex-row">
                   <div class="col-md-2">
                          <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
                   </div>
                 
                </div>
                <h4>Nu se mai downloadeaza nimica cu click, este doar de afisare bag picioarele</h4>
              </div>
            </div>
        
                             <!-- CONTACT MODAL SCRIPT -->
  <div class="modal fade text-dark" id="contactModal">
    <div class="modal-dialog">
      <div class="modal-content"> 
        <div class="modal-body">
          <img src="img/buletin_romania.jpg" style="width: 100%">
        </div>
      </div>
    </div>
  </div>

  <!-- END CONTACT MODAL -->

          </div>
          <div id="menu2" class="container tab-pane fade"><br>
            <h5>Documente respinse</h5>
             <div class="card text-left card-form">
              <div class="card-body">
               <div class="d-flex flex-row">
                   <div class="col-md-2">
                          <button type="button" class="btn btn-success  cancel-pointer">13.08.2020 <i class="far fa-file-alt"></i></button>
                   </div>
                </div>
                <h6 class="text-danger pt-3">* Documentele de mai sus nu au fost aprobate de catre administrator. Va rugam sa incarcati din nou contractul <a href="#menu-contract-to-show" class="menu-contract-to-show" >aici</a>.</h6>
              </div>
            </div>
          </div>


          <div id="menu-contract" class="container tab-pane fade"><br>
              <h6 class="pb-2">Incarcati contractul</h6>
              <div class="card text-left card-form">
                <div class="card-body">
                 <div class="d-flex flex-row">
                     <div class="col-md-6">
                      <h5 class="text-warning-2">Va multumim! Contractul este in curs de aprobare!</h5>
                      <div class="custom-file mb-2" id="customFile" lang="ro">
                          <input type="file" class="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp">
                          <label class="custom-file-label" for="exampleInputFile">
                            Contract de munca <i class="fas fa-file-contract"></i>
                          </label>
                       </div>
                      <button type="submit" class="btn btn-primary">Trimite spre aprobare</button>
                     </div> 
                  </div>
                 
                </div>
              </div>
            </div>


        </div>
      </div>


@endsection
