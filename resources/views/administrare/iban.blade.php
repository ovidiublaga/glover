@extends('layouts/main')
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
        @if(isset(json_decode(auth()->user()->iban)->status))
          @if(json_decode(auth()->user()->iban)->status == 0)
           <h6 class="text-danger">Contul IBAN nu este aprobat</h6>
          @else
            <h6 class="text-success">Contul IBAN este aprobat</h6>
          @endif  
        @endif
        @if(isset(json_decode(auth()->user()->iban)->iban))
          <br />
          <h6 class="text-success">Contul IBAN curent: {{json_decode(auth()->user()->iban)->iban}}</h6>
        @endif
        <br />
        <button class="btn btn-primary"  data-toggle="modal" data-target="#ibna">Doresc modificarea codului IBAN</button>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ibna" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Schimba codul IBAN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/administrare/iban/schimba" method="POST">
        <div class="modal-body">
            @csrf
            <div class="form-group">
              <label for="iban">Introdu codul IBAN</label>
              <input type="text" required name="iban" class="form-control" id="iban" placeholder="cod IBAN">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">M-am razgandit</button>
          <button type="submit" class="btn btn-primary">Trimite</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
