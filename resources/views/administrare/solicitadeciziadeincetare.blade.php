@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
           <div class="card text-center card-form">
            <div class="card-body text-center">
              <h5 class="mb-3">Solicita decizie de incetare carte de munca <i class="fas fa-file-contract"></i></h5>
         
              <form>
           <!-- CAMPURI CE APAR CAND E SELECTAT REACTIVARE CARTTE DE MUNCA -->

                <div class="d-flex flex-row hidingIt-two">
                  <div class="p-4 align-self-start manual-flex">
                    <p>Data decizieiei de incetare <i class="far fa-calendar-alt"></i> : </p>
                    <div class="form-group ">
                      <input type="text" class="from form-control text-center mb-3" name="from" placeholder="Alegeti data">
                        <input type="submit" value="Trimite" class="btn btn-outline-light btn-block mb-3">
                      <p class="text-warning-2">* Solicitare in curs de aprobare.</p>
                    </div>
                  </div>
                 
                   
               </div>


           <!-- /CAMPURI CE APAR CAND E SELECTAT SUSPENDARE CARTTE DE MUNCA -->


              
              </form>
            </div>
          </div>
    </div>
</div>

@endsection
