@extends('layouts/main')
@section('footer_scripts')
@endsection
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
           <div class="card text-center card-form">
            <div class="card-body text-center">
              <h3 class="mb-3">Solicita suspendare / reactivare carte de munca <i class="fas fa-file-contract"></i></h3>
         
              <form>
              <div class="form-group">
               <label for="sel1">Va rugam sa specificati serviciul dorit: </label>
                <select class="form-control"  style="text-align-last:center; " id="actiune-cerere"  onchange="functieAlegere()">
                 <!--  <option selected="true" disabled="disabled">Choose Tagging</option>     -->
                  <option class="suspendare-carte">Suspendare carte de munca</option>
                  <option class="reactivare-carte">Reactivare carte de munca</option>
                </select>
              </div>
           

           <!-- CAMPURI CE APAR CAND E SELECTAT SUSPENDARE CARTTE DE MUNCA -->
                  <div class="d-flex flex-row hidingIt ">
                  <div class="p-4 align-self-start manual-flex">
                    <p>Data inceput <i class="far fa-calendar-alt"></i> : </p>
                    <div class="form-group ">
                      <input type="text" class="from form-control text-center" name="from" placeholder="Alegeti data">
                  
                    </div>
                  </div>
                 
                   <div class="p-4 align-self-start manual-flex">
                    <p>Perioada <i class="fas fa-user-clock"></i> : </p>
                    <div class="form-group ">
                      <input type="text" class="form-control  text-center" name="to" placeholder="Introduceti perioada">
                    </div>
                  </div>
               </div>

          <!-- /CAMPURI CE APAR CAND E SELECTAT SUSPENDARE CARTTE DE MUNCA -->

           <!-- CAMPURI CE APAR CAND E SELECTAT REACTIVARE CARTTE DE MUNCA -->

                <div class="d-flex flex-row hidingIt-two none-displaying ">
                  <div class="p-4 align-self-start manual-flex">
                    <p>Data inceput reactivare <i class="far fa-calendar-alt"></i> : </p>
                    <div class="form-group ">
                      <input type="text" class="from form-control text-center" name="from" placeholder="Alegeti data">
                  
                    </div>
                  </div>
                 
                   
               </div>


           <!-- /CAMPURI CE APAR CAND E SELECTAT SUSPENDARE CARTTE DE MUNCA -->


                <input type="submit" value="Trimite" class="btn btn-outline-light btn-block">
              </form>
            </div>
          </div>
    </div>
</div>
@endsection
