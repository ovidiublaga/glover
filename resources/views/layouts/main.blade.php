<!DOCTYPE html>
<head>
    <title>devino Glover</title>
    <meta charset="UTF-8">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    

    @yield('header_scripts')
</head>

<body>
  <!-- <div class="menu-btn">
    <i class="fas fa-bars fa-2x"></i>
  </div> -->
    <div class="container">
        <!-- Showcase -->
        <header class="showcase-new">
       <!-- Nav -->
         <div class="layertop-new">
          <!-- <div> -->
            <nav class="main-nav">
              <div class="iconbar">
                <i class="fas fa-bars fa-2x"></i>
              </div>
              <div>
                <a href="/">
                  <img src="/img/devinoGlover.ro.png" alt="Devino Glover" class="logo">
                </a>
              </div>
              @include('inc/meniu_stanga')
        
              <ul class="right-menu">
                <!-- <li>
                  <a href="#">
                    <i class="fas fa-search"></i>
                  </a>
                </li> -->
                    @auth
                      <a href="/home" class="contfirst">
                        Contul Meu
                      </a> 
                    @else
                      <a href="/login" class="contfirst">
                       Autentificare
                      </a> 
                    @endauth
               
              </ul>
            </nav>
          </div>
            <!-- /NAV-->

        </header>
        <div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
          <div class="container mt-3" style="padding-top: 10vh">
                @include('inc/messages')
          </div>
        </div>
       @yield('content')
       <div style="height: 30vh !important">

       </div>
    </div>
       @include('inc/footer')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="/js/app.js"></script>
        @yield('footer_scripts')
   
</body>

</html>