@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                     @endif

                You are logged in!<br>
                        <a href="/posts/create">Create post</a>
                        <br>
                        <a href="/documente/create">Introduceti documente personale</a>
                        <br>
                        <a href="/documentemasina/create">Introduceti documentele masinei personaonale</a>
                    <h3>Your Blog Posts</h3>
{{--                     @if (count($posts) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Title</th>
                                <th> </th>
                                <th> </th>
                            </tr>
                            @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td><a href="/posts/{{$post->id}}/edit" class="btn btn-succes">Edit</a></td>
                                <td> 
                                    {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Delete', ['class' => "btn btn-danger"])}}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                        @else
                        <p>You have no posts</p>
                    @endif --}}
                        <br><br><hr><br>
                    @if (count($documents) > 0)
                        <table class="table table-striped">
                            <tr>
                                <th>Carte de indentitate</th>
                                <th>Adeverinta de medic</th>
                                <th>Adeverinta student / Diploma</th>
                                <th>Cazier judiciar</th>
                                <th> Edit</th>
                                <th> Sterge</th>
                            </tr>


                    
                                @foreach($documents as $document)
                                <tr>
                                    <td><img src="/storage/dcs/{{$document->carte_de_identitate}}" style="width: 70%"></td>
                                    <td><img src="/storage/dcs/{{$document->adeverinta_de_medic}}" style="width: 70%"></td>
                                    <td><img src="/storage/dcs/{{$document->diploma}}" style="width: 70%"></td>
                                    <td><img src="/storage/dcs/{{$document->cazier_judiciar}}" style="width: 70%"></td>
                                    <td><a href="/documente/{{$document->id}}/edit" class="btn btn-succes">Edit</a></td>
                                <td> 
                                    {!! Form::open(['action' => ['DocumentsController@destroy', $document->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Delete', ['class' => "btn btn-danger"])}}
                                    {!! Form::close() !!}
                                </td>
                                </tr>
                                @endforeach
                            </table>
                        @else
                        <p>You have no documents</p>
                    @endif





                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
