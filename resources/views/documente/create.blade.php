@extends('layouts.app')
@section('content')
    <h1>Create Post</h1>
    {!! Form::open(['action' => 'DocumentsController@store', 'method' => 'POST',  'enctype' => 'multipart/form-data']) !!}
    {{-- <div class="form-group">
    	{{Form::label('title', 'Title')}}
    	{{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div> --}}

    <hr>
    <div class="form-group">
        <label>Carte de identitate (Obligatoriu)</label><br>
        {{Form::file('carte_de_identitate')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Adeverinta medicala (Obligatoriu)</label><br>
        {{Form::file('adeverinta_de_medic')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Adeverinta de student / Diploma de studiu  (Obligatoriu)</label><br>
        {{Form::file('diploma')}}
    </div>   
    <hr>


    <hr>
    <div class="form-group">
        <label>Cazier judiciar</label><br>
        {{Form::file('cazier_judiciar')}}
    </div>   
    <hr>


    {{Form::submit('Submit', ['class' => "btn btn-primary"])}}
    {!! Form::close() !!}
@endsection