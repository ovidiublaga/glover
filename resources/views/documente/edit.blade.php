@extends('layouts.app')
@section('content')
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['DocumentsController@update', $document->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
    
    <div class="form-group">
        <img src="/storage/dcs/{{$document->carte_de_identitate}}" style="width: 10%">
        {{Form::file('carte_de_identitate')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcs/{{$document->adeverinta_de_medic}}" style="width: 10%">
        {{Form::file('adeverinta_de_medic')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcs/{{$document->diploma}}" style="width: 10%; height: 2% !important;">
        {{Form::file('diploma')}}
    </div>
    <div class="form-group">
        <img src="/storage/dcs/{{$document->cazier_judiciar}}" style="width: 10%">
        {{Form::file('cazier_judiciar')}}
    </div>

    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => "btn btn-primary"])}}
    {!! Form::close() !!}
@endsection