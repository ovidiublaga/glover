@extends('layouts.app')
@section('content')
    <a href="/documente">Go back</a>
     
<table>
    <tr>
        <td><img src="/storage/dcs/{{$document->carte_de_identitate}}" style="width: 70%"></td>
        <td><img src="/storage/dcs/{{$document->adeverinta_de_medic}}" style="width: 70%"></td>
        <td><img src="/storage/dcs/{{$document->diploma}}" style="width: 70%"></td>
        <td><img src="/storage/dcs/{{$document->cazier_judiciar}}" style="width: 70%"></td>
    </tr>
</table>
    
    <hr>
    <small>Written on {{$document->created_at}} by {{$document->user->name}}</small>

    @if(!Auth::guest())
        @if(Auth::user()->id == $document->user_id)
            <a href="/documente/{{$document->id}}/edit">Edit</a>
            {!! Form::open(['action' => ['DocumentsController@destroy', $document->id], 'method' => 'POST', 'class' => 'pull-right']) !!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => "btn btn-danger"])}}
            {!! Form::close() !!}
        @endif
    @endif
    
@endsection