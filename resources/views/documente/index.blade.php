@extends('layouts.app')
@section('content')
    <h1>Posts</h1>
    @if(count($documents) > 0)
        @foreach ($documents as $document)
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcs/{{$document->carte_de_identitate}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcs/{{$document->adeverinta_de_medic}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcs/{{$document->diploma}}" style="width: 70%">
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="/storage/dcs/{{$document->cazier_judiciar}}" style="width: 70%">
            </div>
        </div>
            <div>
                <a href="documente/{{$document->id}}">Vezi situatia</a>
            
            <small>Written on {{$document->created_at}} </small>
            <small>Scris by {{$document->user->name}}</small>
            </div>
        @endforeach
        {{-- {{$documents->links()}} --}}
    @else
        <p>No images found</p>
    @endif
@endsection