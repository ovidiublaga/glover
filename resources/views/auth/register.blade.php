@extends('layouts/main')

@section('content')

                    <div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
        <div class="card text-left card-form">
            <div class="card-body">
            <h3 style="text-align:center;">Autentificare</h3>
            <p style="text-align:center;">Va rugam sa completati campurile de mai jos pentru autentificare.</p>
            <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nume</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  autocomplete="email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Parola</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password"  autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirma Parola</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">Telefon</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" >
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="mod_de_livrare" class="col-md-4 col-form-label text-md-right">Mod de livrare</label>
                        
                            <div class="col-md-6">
                                {{-- <input id="phone" type="text" class="form-control" name="phone" required autocomplete="phone"> --}}
                                <select id="mod_de_livrare" class="form-control" name="mod_de_livrare">
                                    <option>Masina</option>
                                    <option>Bicicleta</option>
                                    <option>Scooter</option>
                                    <option>Pe jos</option>
                                </select>
                            </div>
                        </div>
                            <div class="form-group row" style="display: flex; justify-content: center;">
                                <div class="col-md-6">
                                    <input type="checkbox" id="terms_and_conditions" name="terms_and_conditions">
                                    <label for="terms_and_conditions">Sunt de acord cu</label> 
                                    <a href="/termeni" style="text-decoration:underline; color:blue;">termenii si conditiile de utilizare pentru utilizarea aplicatiei noastre</a>
                                    
                                    
                                </div>
                             
                            </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                       
                            <a class="mt-3" href="/login" style="float:right; text-decoration:underline;">
                                <strong>Am deja cont</strong>
                            </a>
                    </form>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection