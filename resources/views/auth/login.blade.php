@extends('layouts/main')
@section('content')
<div class="col-lg-8 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">
    <div class="container mt-3">
        <div class="card text-left card-form">
            <div class="card-body">
            <h3 style="text-align:center;">Autentificare</h3>
            <p style="text-align:center;">Va rugam sa completati campurile de mai jos pentru autentificare.</p>
                <form method="POST" action="{{ route('login') }}">
                        @csrf
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email *">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Parola *">
                    </div> 
                    <div class="form-check mb-2">
                        <label class="form-check-label" for="check1">
                            <input type="checkbox" class="form-check-input" id="check1" name="remember" value="something" checked>
                            Tine-ma minte
                        </label>
                    </div>
                    <input type="submit" value="Trimite" class="btn btn-outline-light btn-block mb-2">
                    @if (Route::has('password.request'))
                        <a class="mt-3" href="{{ route('password.request') }}" style="float:left; text-decoration:underline;">
                            <strong>Am uitat parola!</strong>
                        </a>
                    @endif
                    <a class="mt-3" href="/register" style="float:right; text-decoration:underline;">
                        <strong>Nu am cont</strong>
                    </a>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
