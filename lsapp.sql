-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2020 at 07:02 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `documente`
--

CREATE TABLE `documente` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(1) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `documentemasina`
--

CREATE TABLE `documentemasina` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `buletin_proprietar_masina` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asigurare_masina` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `certificat_inmatriculare` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contract_de_comodat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `carte_de_identitate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adeverinta_de_medic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diploma` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cazier_judiciar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `carte_de_identitate`, `adeverinta_de_medic`, `diploma`, `cazier_judiciar`, `created_at`, `updated_at`, `user_id`) VALUES
(3, 'Romanian_Identity_Card_2009_1582551589.jpg', 'TIP0001-1-e1504043780277_1582551589.jpg', 'diploma-de-licenta-1-638_1582551589.jpg', 'noimage.jpg', '2020-02-24 11:39:49', '2020-02-24 11:39:49', 1),
(4, 'Romanian_Identity_Card_2009_1582615950.jpg', 'imaginea-cu-care-eugenie-bouchard-i-a-innebunit-pe-toti-galerie-foto-cu-cea-mai-sexy-jucatoare-din-circuitul_1582717999.png', 'Genis-Bouchard_1582718017.png', 'lux_club_piatra_1582702265.jpeg', '2020-02-25 05:32:30', '2020-02-26 09:53:37', 1),
(7, 'Romanian_Identity_Card_2009_1582616000.jpg', 'TIP0001-1-e1504043780277_1582616000.jpg', 'diploma-de-licenta-1-638_1582616000.jpg', 'noimage.jpg', '2020-02-25 05:33:20', '2020-02-25 05:33:20', 7),
(8, 'Romanian_Identity_Card_2009_1582622425.jpg', 'TIP0001-1-e1504043780277_1582622425.jpg', 'diploma-de-licenta-1-638_1582622425.jpg', '1acb48de00ef616a5dc3b38981109089_1582622425.jpg', '2020-02-25 07:20:25', '2020-02-25 07:20:25', 7);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_14_094558_create_posts_table', 1),
(5, '2020_02_17_132135_add_user_id_to_posts', 2),
(6, '2020_02_18_121912_add_cover_image_to_posts', 3),
(7, '2020_02_24_070810_add_terms_and_conditions_to_user_registration', 4),
(8, '2020_02_24_074057_add_phone_to_users', 5),
(9, '2020_02_24_074615_add_mod_de_livrare_to_users', 6),
(10, '2020_02_24_085850_create_documente_personale_table', 7),
(11, '2020_02_24_093929_create_documents_table', 8),
(12, '2020_02_24_140627_add_user_id_to_documents', 9),
(13, '2020_02_26_083155_create_metodalivrares_table', 10),
(14, '2020_02_26_084147_create_documentemasinas_table', 11),
(15, '2020_02_26_085046_add_user_id_to_documentemasina', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ovidiublaga1@gmail.com', '$2y$10$DeJlVJdD7tQNVPS5gUEIf.mZLhluuY32G6dM/XI0QnZ7n6FGoUd0K', '2020-03-04 11:25:04');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created_at`, `updated_at`, `user_id`, `cover_image`) VALUES
(16, 'sds', '<p>dsd</p>', '2020-02-18 10:48:08', '2020-02-18 10:48:08', 1, 'noimage.jpg'),
(18, 'marla', '<p>marlan 2</p>', '2020-02-18 13:53:18', '2020-02-18 13:53:18', 1, '74187142_3119585164780968_3513853731454058496_o_1582041198.jpg'),
(20, 'sdsds', '<p>dssdsdsdsd</p>', '2020-02-26 06:02:24', '2020-02-26 06:02:24', 7, 'lux_club_piatra_1582704144.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `terms_and_conditions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mod_de_livrare` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'masina'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `terms_and_conditions`, `phone`, `mod_de_livrare`) VALUES
(1, 'ovidiu', 'ovidiublaga1@gmail.com', NULL, '$2y$10$vwoCilEYDySoDGwmnoYDmOU2WNZ1WyR1OY8xXGxvffdzChkXBEHuG', 'GmuUbAgEOc4xc1CYsAKc3TVqY8XN834nkgmFknC4Q2wYsnL8w3TnSqNXBkq3', '2020-02-17 09:36:48', '2020-02-17 09:36:48', '', '', 'masina'),
(2, 'Daniel', 'Daniel@test.ro', NULL, '$2y$10$Ms0Onoc.ie6aufsoQZHaNuYl5jVeh/RRF.EsLnQvp0dF6zKEk2xK6', NULL, '2020-02-17 10:40:18', '2020-02-17 10:40:18', '', '', 'masina'),
(3, 'marcel', 'marcel@test.ro', NULL, '$2y$10$dVciEwoMR38WXyMM4gnkoO6AjLZxnDxoYkYTeyj/2yuLy.jWQk1DS', 'QGlYGXWzzO2uIn5k3MgGtwE4zn1DqMn5dtvs4S4ItCqdrhVeGblz8tME0tF5', '2020-02-18 07:31:09', '2020-02-18 07:31:09', '', '', 'masina'),
(4, 'zcofeina', 'zcofeina@gmail.com', NULL, '$2y$10$qDxQ86FIC9pjkblWrU1wdeuWlVYiZkHlBi4PN/DptjAvRmer6SUJ.', NULL, '2020-02-24 05:33:34', '2020-02-24 05:33:34', '1', '', 'masina'),
(5, 'HASMAS', 'hasmas@daniel.ro', NULL, '$2y$10$ThhjLta2y83zybCmQI69buhqiZq8DelTZjdAkz9TYPP9JItFQ9HSq', NULL, '2020-02-24 06:09:13', '2020-02-24 06:09:13', '1', '0722334455', 'masina'),
(6, 'georgian', 'georgian@test.ro', NULL, '$2y$10$lmvdp1ZbCr0ZPjDMZgbn3uoxPjri7vfD7TPAETs2gZjIqCKsG1KyC', NULL, '2020-02-24 06:37:48', '2020-02-24 06:37:48', '1', '02233441122', 'Pe jos'),
(7, 'tzucurel', 'tzucurel@test.ro', NULL, '$2y$10$Y7HYo3vDxOBjR3qlX9knqe2AxJHa.dck3npEaVhYZgximBve5xXVC', NULL, '2020-02-25 06:48:35', '2020-02-25 06:48:35', '1', '223445522', 'Scooter');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `documentemasina`
--
ALTER TABLE `documentemasina`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `documentemasina`
--
ALTER TABLE `documentemasina`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
