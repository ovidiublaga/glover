document.querySelector('.iconbar').addEventListener('click', () => document.querySelector('.main-menu').classList.toggle('show'));

// Codul pentru afisare in input in momentul in care selectez o imagine;
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$(document).ready(function(){
  $(".nav-tabs a").click(function(){
    $(this).tab('show');
  });
});
//DATETIMEPICKER FUNCTION
  $( function() {
    var dateFormat = "dd/mm/yy",
      from = $( ".from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat: 'dd-mm-yy'
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy'
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }

      return date;
    }
  } );

  // document.querySelector('.menu-contract-to-show').addEventListener('click', () => document.querySelector('#menu-contract').classList.toggle('active'));
  // document.querySelector('.menu-contract-to-show').addEventListener('click', () => document.querySelector('#menu-contract').classList.toggle('show'));
              
  // document.querySelector('.menu-contract-to-show-first').addEventListener('click', () => document.querySelector('#menu-contract').classList.toggle('active'));
  // document.querySelector('.menu-contract-to-show-first').addEventListener('click', () => document.querySelector('#menu-contract').classList.toggle('show'));